export class Message
{
  authorId: string;
  content: string;
  tag: string;

  constructor(authorId,content,tag)
  {
      this.authorId = authorId;
      this.content = content;
      this.tag = tag;
  }
}
