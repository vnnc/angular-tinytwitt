import { Component, OnInit,Input } from '@angular/core';
import { Message } from '../classes/message';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() message: Message;

  chronoStart: number;

  chronoTime: number;

  constructor(
    private usersrv : UserService,
    private router: Router
  ) { }

  onClickFollow()
  {
    this.chronoStart = Date.now();
    this.usersrv.followUser(this.message.authorId).subscribe(response=>this.handleFollowResponse(response));
  }

  onClickTimeline()
  {
    this.router.navigate(["/timeline/"+this.message.authorId]);
  }

  handleFollowResponse(response)
  {
      this.chronoTime = Date.now() - this.chronoStart;
      console.log("temps chargement follow: ",this.chronoTime);
      console.log("follow response: ",response);
  }

  ngOnInit() {
  }

}
