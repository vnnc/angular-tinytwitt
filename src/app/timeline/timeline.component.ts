import { Component, OnInit,Input } from '@angular/core';
import { TimelineService } from '../timeline.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Message } from '../classes/message';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  @Input() self: boolean = false;

  messageLimit: string = "10";

  messages: Message[];

  chronoStart: number;

  chronoTime: number;

  constructor(
    private timelinesrv : TimelineService,
    private route : ActivatedRoute,
    private auth: AuthService
  )
  {
  }


  getTimeline(user: string)
  {
      this.chronoStart = Date.now();
      this.timelinesrv.getTimeline(user,this.messageLimit).subscribe(timeline =>
        this.buildTimeline(timeline)
      );
  }

  buildTimeline(timeline)
  {
      this.chronoTime = Date.now() - this.chronoStart;
      console.log("Request time: "+this.chronoTime);
      if(timeline!=null)
      {
        //console.log("type de la reponse: "+typeof(timeline));
        //let timelineArray = JSON.parse(timeline);
        //console.log(timelineArray);
        this.messages = timeline.items.map(msg => {
          return new Message(msg.autor,msg.content,msg.hashTag);
        })
      }
  }

  ngOnInit() {
    this.loadTimeline();
  }

  loadTimeline()
  {
    if(this.self)
    {
      console.log("user in auth:",this.auth.getUser());
      this.getTimeline(this.auth.getUser().id);
    }else{
      this.route.params.subscribe(params => this.getTimeline(params["user"]));
    }
  }

  onClickValidateLimit()
  {
    this.loadTimeline();
  }

}
