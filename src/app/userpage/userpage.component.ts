import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {

  userToFollow: string = "";

  chronoTime: number;

  chronoStart: number;

  tagToSee: string = "";

  constructor(
    private usersrv : UserService,
    private registersrv: RegisterService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onClickFollow()
  {
    this.chronoStart = Date.now();
    this.usersrv.followUser(this.userToFollow).subscribe(response => this.handleFollowResponse(response));
  }

  handleFollowResponse(response)
  {
    this.chronoTime = Date.now() - this.chronoStart;
    console.log("temps chargement follow: ",this.chronoTime);
    console.log("follow response: ",response);
  }

  onClickClean(){
    this.usersrv.clean().subscribe(response=>{});
  }

  onClickScalable(){
    console.log("clicked scalable");
    this.usersrv.createUserForScalableTests().subscribe(response=>{});
    // this.registersrv.createUser({id:"userFollow100",key:""}).subscribe(r=>{});
    // this.registersrv.createUser({id:"userFollow1000",key:""}).subscribe(r=>{});
    // this.registersrv.createUser({id:"userFollow5000",key:""}).subscribe(r=>{});
    //
    // let that = this;
    //
    // for(let i=0;i<5000;i++)
    // {
    //   this.registersrv.createUser({id:"user"+i,key:""}).subscribe(r=>{
    //     if(i<100)
    //     {
    //       that.usersrv.followUserAlt("userFollow100","user"+i).subscribe(r=>{});
    //       that.usersrv.followUserAlt("user"+i,"userFollow100").subscribe(r=>{});
    //     }
    //     if(i<1000)
    //     {
    //       that.usersrv.followUserAlt("userFollow1000","user"+i).subscribe(r=>{});
    //       that.usersrv.followUserAlt("user"+i,"userFollow1000").subscribe(r=>{});
    //     }
    //     that.usersrv.followUserAlt("userFollow5000","user"+i).subscribe(r=>{});
    //     that.usersrv.followUserAlt("user"+i,"userFollow5000").subscribe(r=>{});
    //   });
    // }
    //
    // setTimeout(function(){
    //   for(let i=0;i<100;++i)
    //   {
    //     that.usersrv.postMessage({authorId:"user"+i,content:"message"+i,tag:"hashTag"}).subscribe(r=>{});
    //   }
    //
    //   for(let i=0;i<1000;++i)
    //   {
    //     that.usersrv.postMessage({authorId:"user2500",content:"message"+i,tag:"hashTag1000"}).subscribe(r=>{});
    //   }
    //
    //   for(let i=0;i<5000;++i)
    //   {
    //     that.usersrv.postMessage({authorId:"user4900",content:"message"+i,tag:"hashTag5000"}).subscribe(r=>{});
    //   }
    // },120000);
  }

  onClickTag()
  {
    this.router.navigate(["/tag/"+this.tagToSee]);
  }

}
