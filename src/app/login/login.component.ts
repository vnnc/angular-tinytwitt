import { Component, OnInit } from '@angular/core';
import { User } from '../classes/user';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = {
    id: '',
    key: ''
  };

  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  onClickRegister(){
    this.router.navigate(["/register"]);
  }

  onClickLogin(){
    // Bypass
    console.log("login clicked");
    this.auth.setUser(this.user);
    console.log("user in auth: ",this.auth.getUser().id);
    this.router.navigate(["/my"]);
  }
  ngOnInit() {
  }

}
