import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { AuthService } from '../auth.service';
import { Message } from '../classes/message';

@Component({
  selector: 'app-post-message',
  templateUrl: './post-message.component.html',
  styleUrls: ['./post-message.component.css']
})
export class PostMessageComponent implements OnInit {

  chronoStart: number;

  chronoTime: number;

  message: Message = {
    authorId: this.auth.getUser().id,
    content: '',
    tag: ''
  };

  constructor(
    private usersrv : UserService,
    private auth : AuthService
  ) { }

  ngOnInit() {
  }

  onClickSend()
  {
    console.log("send message clicked");
    this.chronoStart = Date.now();
    this.usersrv.postMessage(this.message).subscribe(response => this.handlePostResponse(response));
  }

  handlePostResponse(response)
  {
    this.chronoTime = Date.now() - this.chronoStart;
    console.log("temps chargement creation de message: ",this.chronoTime);
    console.log("message posted, response: ",response);
  }
}
