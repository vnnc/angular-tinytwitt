import { Component, OnInit } from '@angular/core';
import { TimelineService } from '../timeline.service';
import { ActivatedRoute } from '@angular/router';
import { Message } from '../classes/message';


@Component({
  selector: 'app-tagline',
  templateUrl: './tagline.component.html',
  styleUrls: ['./tagline.component.css']
})
export class TaglineComponent implements OnInit {

  messageLimit: string = "10";

  chronoStart: number;

  chronoTime: number;

  messages: Message[];

  constructor(
    private timelinesrv : TimelineService,
    private route : ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadTagline();
  }

  getTagline(tag: string)
  {
      this.chronoStart = Date.now();
      this.timelinesrv.getTagline(tag,this.messageLimit).subscribe(tagline =>
        this.buildTagline(tagline)
      );
  }

  buildTagline(tagline)
  {
      this.chronoTime = Date.now() - this.chronoStart;
      console.log("Request time: "+this.chronoTime);
      if(tagline!=null)
      {
        //console.log("type de la reponse: "+typeof(timeline));
        //let timelineArray = JSON.parse(timeline);
        //console.log(timelineArray);
        this.messages = tagline.items.map(msg => {
          return new Message(msg.autor,msg.content,msg.hashTag);
        })
      }
  }

  loadTagline()
  {
    this.route.params.subscribe(params => this.getTagline(params["tag"]));
  }

  onClickValidateLimit()
  {
    this.loadTagline();
  }
}
