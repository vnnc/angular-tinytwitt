import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimelineComponent } from './timeline/timeline.component';
import { ProfileComponent } from './profile/profile.component';
import { TaglineComponent } from './tagline/tagline.component';
import { UserpageComponent} from './userpage/userpage.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },

  { path: 'login',
    component: LoginComponent },

  { path: 'my',
    component: UserpageComponent
  },

  { path: 'register',
    component: RegisterComponent
  },

  { path: 'timeline/:user',
    component: TimelineComponent
  },

  { path: 'profile',
    component: ProfileComponent
  },

  { path: 'tag/:tag',
    component: TaglineComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
