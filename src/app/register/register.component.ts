import { Component, OnInit } from '@angular/core';
import { User } from '../classes/user';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = {
    id: '',
    key: ''
  };

  constructor(
    private registersrv: RegisterService,
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  onClickValidate(){
    this.registersrv.createUser(this.user).subscribe(response => this.afterRegistration(response));
  }

  afterRegistration(response)
  {
    this.auth.setUser(this.user);
    console.log("user in auth: ",this.auth.getUser());
    this.router.navigate(["/my"]);
  }
}
