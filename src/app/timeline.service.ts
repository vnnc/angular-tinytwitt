import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  constructor(
    private http: HttpClient
  ) { }

  getTimeline(id: string,limit: string): Observable<string>
  {
    console.log(" id passed to timeline: ",id);
    let postvars = {
      idUser: id,
      limit: limit
    }
    return this.http.post<any>(environment.serverUrl+"services/getTimeLineWithLimit",postvars);
  }

  getTagline(tag: string,limit:string): Observable<string>
  {
    let postvars = {
      hashTag: tag,
      limit: limit
    }
    return this.http.post<any>(environment.serverUrl+"services/getHashtagWithLimit",postvars);
  }
}
