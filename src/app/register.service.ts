import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './classes/user';
import {environment} from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient
  ) { }

  createUser(user: User): Observable<User> {
    let postvars = {
      name: user.id
    }
    console.log(user);
    console.log(postvars);
    return this.http.post<User>(environment.serverUrl+"services/createUser",postvars);
  }

}
