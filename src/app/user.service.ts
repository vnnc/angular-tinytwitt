import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Message } from './classes/message';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  postMessage(message: Message){
    let postvars = {
      idUser: message.authorId,
      message: message.content,
      hashTag: message.tag
    }
    console.log("sending message with vars: ",postvars);
    return this.http.post<Message>(environment.serverUrl+"services/postMessage",postvars);
  }

  followUser(id: string){
    let postvars = {
      idUser: this.auth.getUser().id,
      newIdUserFollow: id
    }
    return this.http.post<Message>(environment.serverUrl+"services/followUser",postvars);
  }

  followUserAlt(id: string,idToFollow: string){
    let postvars = {
      idUser: id,
      newIdUserFollow: idToFollow
    }
    return this.http.post<Message>(environment.serverUrl+"services/followUser",postvars);
  }

  clean()
  {
    return this.http.post<string>(environment.serverUrl+"services/clean",{});
  }

  createUserForScalableTests()
  {
    return this.http.post<string>(environment.serverUrl+"services/createUserForScalableTests",{});
  }
}
